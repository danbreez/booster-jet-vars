### Important
- This mod requires [AceCoreLib](https://gitlab.com/accensi/hd-addons/acecorelib).
- It *also* requires [Booster Jets](https://gitlab.com/accensi/hd-addons/booster-jets).

### Usage
---
- Use Item: Turn on.
- Use + Use Item: If turned off, enable charging.
- Jump: If turned on, gain boost to current momentum.
  - If you're running the "Freeman" variant: Crouch+Jump to ~~crouchjump~~ trade vertical boost for horizontal boost.
  - If you're running the "Pilot" variant: Crouch+Jump to get vertical.

### Notes
---
- Cost scales with how encumbered you are.
- You can and probably will incap yourself with it so watch out.
- Variant jets are found naturally in the world. Pretty sure the Merchant won't have them, though.
- Loadout code is `bsf` for the Freeman version and `bsp` for the Pilot version.